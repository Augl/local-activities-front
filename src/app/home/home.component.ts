import { Component, OnInit } from '@angular/core';
import { Club, ClubDataService } from 'src/shared/services/club-data.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  public clubs: Club[]

  constructor(private clubService: ClubDataService) { 
    this.clubs = this.clubService.getClubs();
  }

  ngOnInit() {
  }

}
