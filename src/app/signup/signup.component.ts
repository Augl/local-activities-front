import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { CategoryDataService, Category } from 'src/shared/services/category-data.service';
import { Observable } from 'rxjs';
import {map, startWith} from 'rxjs/operators';
import { Router } from '@angular/router';


@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  public passwordHidden = true;

  public clubUserForm = new FormGroup({
      email: new FormControl('', Validators.required),
      password: new FormControl('', Validators.required)
  });

  public clubDataForm = new FormGroup({
    name: new FormControl('', Validators.required),
    description: new FormControl('', Validators.required),
    category: new FormControl('', Validators.required),
    location: new FormControl('', Validators.required),
    ageMin: new FormControl('', Validators.required),
    ageMax: new FormControl('', Validators.required),
    genderFemale: new FormControl(false, Validators.required),
    genderMale: new FormControl(false, Validators.required),
    values: new FormControl(''),
    milestones: new FormControl(''),
    prices: new FormControl(''),
    contact: new FormControl('')
  });

  public categories: Category[];
  public filteredCategories: Observable<Category[]>;

  constructor(private categoryService: CategoryDataService, private router: Router) { 
    // load categories
    this.categories = this.categoryService.getCategories();
  }

  ngOnInit() {
    this.filteredCategories = this.clubDataForm.get("category").valueChanges
      .pipe(
        startWith(''),
        map(value => {
          const filterValue = value.toLowerCase();
          return this.categories.filter(option => option.name.toLowerCase().includes(filterValue));
        })
      );
  }

  onSubmitClubUser() {
    console.log(this.clubUserForm.value);
  }

  onSubmitClubData() {
    console.log(this.clubDataForm.value);
    this.router.navigate(["club", 1]);
  }

}
