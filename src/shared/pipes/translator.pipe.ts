import { Pipe, PipeTransform } from '@angular/core';
import { TranslatorService } from '../services/translator.service';

@Pipe({
  name: 'translator',
  pure: false
})
export class TranslatorPipe implements PipeTransform {

  constructor(private ts: TranslatorService) {
  }

  public transform(value: any, args?: any): any {
    return this.ts.translate(value);
  }

}
