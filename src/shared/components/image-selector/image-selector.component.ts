import { Component, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-image-selector',
  templateUrl: './image-selector.component.html',
  styleUrls: ['./image-selector.component.css']
})
export class ImageSelectorComponent implements OnInit {

  public images: File[] = [];

  public imagePaths: string[] = [];

  constructor(private sanitizer: DomSanitizer) { }

  ngOnInit() {
  }

  onValueChange(event: any) {
    console.log(event.target.files);
    Array.from(event.target.files).forEach((img: File) => {
      let previewImagePath = URL.createObjectURL(img);
      this.imagePaths.push(this.sanitizer.bypassSecurityTrustUrl(previewImagePath) as string);
      this.images.push(img);
    });
  }

  deleteImage(index: number) {
    this.imagePaths.splice(index, 1);
    this.images.splice(index, 1);
  }
}
