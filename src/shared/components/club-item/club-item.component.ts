import { Component, Input, ViewChild, AfterViewInit, ElementRef } from '@angular/core';
import { Club } from 'src/shared/services/club-data.service';

@Component({
  selector: 'app-club-item',
  templateUrl: './club-item.component.html',
  styleUrls: ['./club-item.component.css']
})
export class ClubItemComponent implements AfterViewInit {

  @Input()
  public club: Club;

  @ViewChild("imageWrapper")
  private imageWrapper: ElementRef

  constructor() { }

  ngAfterViewInit() {
    console.log(this.imageWrapper.nativeElement.offsetWidth)
  }

}
