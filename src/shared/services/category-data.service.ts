import { Injectable } from '@angular/core';

export interface Category {
  name: string;
}

@Injectable({
  providedIn: 'root'
})
export class CategoryDataService {

  constructor() { }

  getCategories(): Category[] {
    return [{name: "Volleyball"}, {name: "Fussball"}];
  }

}
