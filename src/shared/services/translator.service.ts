import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class TranslatorService {

  private lang: any;

  private _language: string;
  public get language(): string {
    return this._language;
  }

  constructor(private http: HttpClient) {
    this._language = "de";
    this.loadLanguage("de");
  }

  public translate(id: string): string {
    if (!this.lang) return "";
    return this.lang[id] || "";
  }

  public loadLanguage(lang: string) {
    this.http.get("../../assets/lang/" + lang + ".json").subscribe((data: Map<string, string>) => {
      this.lang = data;
      console.log("Language loaded: ", lang)
    });
  }

}
