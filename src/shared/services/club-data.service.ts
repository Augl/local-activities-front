import { Injectable } from '@angular/core';

export interface Club {
  name: string;
  sport: string;
  distance: number;
}

@Injectable({
  providedIn: 'root'
})
export class ClubDataService {

  constructor() { }

  getClubs(): Club[] {
    return [
      {
        name: "Volley Hünenberg",
        sport: "Volleyball",
        distance: 5
      }, 
      {
        name: "LKZ Volleyball",
        sport: "Volleyball",
        distance: 7
      }, 
      {
        name: "Tennisclub Baar",
        sport: "Tennis",
        distance: 8
      }, 
      {
        name: "FC Cham",
        sport: "Fussball",
        distance: 19
      }, 
      {
        name: "Zug 94",
        sport: "Fussball",
        distance: 30
      }];
  }

}
